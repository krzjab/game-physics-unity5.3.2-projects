﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[DisallowMultipleComponent]
//Symulations Physics Engine
public class SymPE : MonoBehaviour {

	//draw forces init start
	public bool showTrails = false;

	//private List<Vector3> forceVectorList = new List<Vector3>();
	private LineRenderer lineRenderer;
	private int numberOfForces;
	//draw forces init end


	public float mass;
	public Vector3 velocityVector;
	public Vector3 accelerationVector;
	public Vector3 netForceVector;
	public Vector3 inputForce;
	public Vector3 inputVelocity;
	public Transform position0;
	private float posX;
	private float posY;
	private Transform positionXY;

	public float FLOOR_const;
	public float Jump_v_const;
	public float maxVelocityHorizontal_const;
	public float minVelocityHorizontal_const;
	public float horizontalAcceleration_const;



	private List<Vector3> forceVectorList = new List<Vector3>();

	public bool right;
	public bool left;
	public bool up;
	public bool down;

	//Friction
	public float FrictionMi;
	public Vector3 g;
	//AirDrag
	public float AirDragK;

	//Collision_Manager Stuff
	//list of colliding objects on the scene:
	public MyCollider[] myColliderTable;


	public Sprite playerSprite;
	public Vector2 playerBox_min;
	public Vector2 playerBox_max;
	public Vector2 playerBox_center;
	public float playerBox_width;
	public float playerBox_height;
	public float playerboxScale; //moltible width and height by this to get a smaler collision box for the player eg. X * 1/2
	public Vector2 playerBox_extend;

	public bool groundCollision;
	public bool lefthandCol;
	public bool righthandCol;
	public bool topCol;


	Vector2 c_Pc_distance;

	public float toPixels;

	// Use this for initialization
	void Start () {
		groundCollision = false;
		playerSprite = gameObject.GetComponent<SpriteRenderer> ().sprite;
		toPixels = playerSprite.pixelsPerUnit;
		PlayerBoxInit ();


		FrictionMi = 0.2f;
		AirDragK = 0.2f;
		g = new Vector3 (10.0f, 0f, 0f); //for friction

		position0 = gameObject.transform;
		DrawForcesStart ();
		mass = 1.0f;

		//FLOOR_const = -3.0f;
		FLOOR_const = GameObject.Find("floor").GetComponent<Transform>().position.y;
		Jump_v_const = 14.0f;
		minVelocityHorizontal_const = 0.5f;
		maxVelocityHorizontal_const = 10.0f;
		horizontalAcceleration_const = 5.0f;

		//make the list of Colliders in the scene:
		myColliderTable = FindObjectsOfType(typeof(MyCollider)) as MyCollider[];
		foreach (MyCollider mcol in myColliderTable) {
			Debug.Log ("test listy MyCollider" + mcol.gameObject.name);
		}

	}

	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate()
	{	
		PlayerBoxUpdate ();
		GetInputForces ();
		DrawForcesUpdate ();

		UpdatePosition();
		CollisionManager ();

		/*
		if (netForceVector == Vector3.zero) {
			transform.position = transform.position + velocityVector * Time.deltaTime;
		} else {
			Debug.LogError ("Unbalanced force detected, help!");
		}
	*/
	}

	public void AddForce (Vector3 forceVector) {
		forceVectorList.Add (forceVector);
	}



	void UpdatePosition()
	{
		// Sum the forces and clear the list
		netForceVector = Vector3.zero;
		netForceVector += inputForce;
		foreach (Vector3 forceVector in forceVectorList) {
			netForceVector = netForceVector + forceVector;
		}
		forceVectorList = new List<Vector3> (); // Clear the list

		// update dynamics x V a
		accelerationVector = netForceVector / mass;
	
		//update velocity
		// Y work on Key Hit so velocity snap is recuaierd

		velocityVector.y = velocityVector.y + accelerationVector.y * Time.deltaTime + inputVelocity.y;
		
		// X work on keyHolding so it gets acceleration
		velocityVector.x = velocityVector.x + accelerationVector.x * Time.deltaTime ;
		//update position
		transform.position = transform.position + velocityVector * Time.deltaTime;

		AirDrag ();
		FrictionLegacy();
		FrictionCol ();

		/*
		//the floor limit of the level Jumping platform
		if (gameObject.transform.position.y < FLOOR_const) {
			//gameObject.transform.Translate (0.0f , 0.01f, 0.0f );
			velocityVector.y = 0;

		}
		*/

	}


	void DrawForcesStart()
	{
		//forceVectorList = GetComponent<PhysicsEngine>().forceVectorList;

		lineRenderer = gameObject.AddComponent<LineRenderer>();
		lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
		lineRenderer.SetColors(Color.yellow, Color.yellow);
		lineRenderer.SetWidth(0.2F, 0.2F);
		lineRenderer.useWorldSpace = false;
	}

	void DrawForcesUpdate () {
		if (showTrails) {
			lineRenderer.enabled = true;
			numberOfForces = forceVectorList.Count;
			lineRenderer.SetVertexCount(numberOfForces * 2);
			int i = 0;
			foreach (Vector3 forceVector in forceVectorList) {
				lineRenderer.SetPosition(i, Vector3.zero);
				lineRenderer.SetPosition(i+1, -forceVector);
				i = i + 2;
			}
		} else {
			lineRenderer.enabled = false;
		}
	}

	void GetInputForces()
	{
		if (Input.GetKey (KeyCode.RightArrow) ) {
			Debug.Log ("Right arrow on the hit");
			//if you whant to disable changeing directions while in Jump then put the if FLOOR statement

			if (groundCollision == true) {
				if (velocityVector.x < minVelocityHorizontal_const) {
					velocityVector.x = minVelocityHorizontal_const;
				}  else if (velocityVector.x > maxVelocityHorizontal_const) {
					velocityVector.x = maxVelocityHorizontal_const;
				}
				inputForce.x = horizontalAcceleration_const;
			} else if (groundCollision == false) {
				inputForce.x = horizontalAcceleration_const / 10.0f;
			}

			//inputVelocity.x = 1;
			right = true;
		} else {
			right = false;
		}

		if (Input.GetKey (KeyCode.LeftArrow)) {
			Debug.Log ("Left arrow on the hit max min velocity");

			if (groundCollision == true) {
				if (velocityVector.x > -minVelocityHorizontal_const) {
					velocityVector.x = -minVelocityHorizontal_const;
				} else if (velocityVector.x < -maxVelocityHorizontal_const) {
					velocityVector.x = -maxVelocityHorizontal_const;
				}
				inputForce.x = -horizontalAcceleration_const;
			} else if (groundCollision == false) {
				inputForce.x = -horizontalAcceleration_const / 10.0f;
			}
			//inputVelocity.x = -1;
			left = true;
		} else {
			left = false;
		}

	

		if (Input.GetButtonDown("JumpBall")) {
			Debug.Log ("UP arrow on the hit");
			//inputForce.y = 0;

			up = true;
			/*
			if (transform.position.y <= FLOOR_const) {
				//inputVelocity.y = Jump_v_const;
				velocityVector.y = Jump_v_const;

			}
			*/
			if (groundCollision == true) {
				//inputVelocity.y = Jump_v_const;
				velocityVector.y = Jump_v_const;
				groundCollision = false;

			}
		} else {
			
			up = false;
		}

		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			Debug.Log("DOWN arrow on the hit");
			//inputForce.y =-1;
		
			down = true;
		} else {
			down = false;
		}


		if ((right || left) == false) {
			inputForce.x = 0;
			inputVelocity.x = 0;
		}

		if ((up || down) == false) {
			//inputForce.y = 0;
			inputVelocity.y = 0;
			inputForce.y = 0;
		}

	}



	void AirDrag()
	{
		//velocityVector.x = velocityVector.x * 0.98f;

		if (lefthandCol == false && righthandCol == false && topCol == false && groundCollision == false) {

			//zmien friction na airdrag
			//X airdrag
			if ((velocityVector.x > 0.1f) || (velocityVector.x < -0.1f ) ) {
				//tarcie
				//velocityVector.x = velocityVector.x + accelerationVector.x * Time.deltaTime ;
				if (velocityVector.x > 0.1f ) {
					velocityVector.x = velocityVector.x - Mathf.Abs(velocityVector.x) * AirDragK * Time.deltaTime;
				} else if (velocityVector.x < -0.1f   ) {
					velocityVector.x = velocityVector.x + Mathf.Abs(velocityVector.x) * AirDragK * Time.deltaTime;
				}

			} else if (velocityVector.x < 0.1f && velocityVector.x > -0.1f &&  !(Input.GetKey (KeyCode.LeftArrow)) && !(Input.GetKey (KeyCode.RightArrow)) ) {
				//brak tarcia
				//velocity = 0.0;
				velocityVector.x = 0.0f;

			}

			//Y airdrag
			if ((velocityVector.y > 0.1f) || (velocityVector.y < -0.1f ) ) {
				//tarcie
				//velocityVector.x = velocityVector.x + accelerationVector.x * Time.deltaTime ;
				if (velocityVector.y > 0.1f ) {
					velocityVector.y = velocityVector.y - Mathf.Abs(velocityVector.y) * AirDragK * Time.deltaTime;
				} else if (velocityVector.y < -0.1f   ) {
					velocityVector.y = velocityVector.y + Mathf.Abs(velocityVector.y) * AirDragK * Time.deltaTime;
				}

			} 

			//zmien frction name AirDrag AirDrag koniec




		}


	}

	void FrictionLegacy()
	{
		if ((velocityVector.x > 0.1f) || (velocityVector.x < -0.1f ) ) {
			//tarcie
			//velocityVector.x = velocityVector.x + accelerationVector.x * Time.deltaTime ;
			if (velocityVector.x > 0.1f && (gameObject.transform.position.y < FLOOR_const+0.2f)) {
				velocityVector.x = velocityVector.x - g.x * FrictionMi * Time.deltaTime;
			} else if (velocityVector.x < -0.1f   && (gameObject.transform.position.y < FLOOR_const+0.2f) ) {
				velocityVector.x = velocityVector.x + g.x * FrictionMi * Time.deltaTime;
			}

		} else if (velocityVector.x < 0.1f && velocityVector.x > -0.1f &&  !(Input.GetKey (KeyCode.LeftArrow)) && !(Input.GetKey (KeyCode.RightArrow)) ) {
			//brak tarcia
			//velocity = 0.0;
			velocityVector.x = 0.0f;

		}
	}
	void FrictionCol()
	{
		if ((velocityVector.x > 0.1f) || (velocityVector.x < -0.1f ) ) {
			//tarcie
			//velocityVector.x = velocityVector.x + accelerationVector.x * Time.deltaTime ;
			if (velocityVector.x > 0.1f && groundCollision == true) {
				velocityVector.x = velocityVector.x - g.x * FrictionMi * Time.deltaTime;
			} else if (velocityVector.x < -0.1f   && groundCollision == true ) {
				velocityVector.x = velocityVector.x + g.x * FrictionMi * Time.deltaTime;
			}

		} else if (velocityVector.x < 0.1f && velocityVector.x > -0.1f &&  !(Input.GetKey (KeyCode.LeftArrow)) && !(Input.GetKey (KeyCode.RightArrow)) ) {
			//brak tarcia
			//velocity = 0.0;
			velocityVector.x = 0.0f;

		}
	}

	void CollisionManager()
	{
		foreach (MyCollider mcol in myColliderTable) {
			//Debug.Log ("test listy MyCollider" + mcol.gameObject.name);

			c_Pc_distance = playerBox_center - mcol.myBox_center; //end player, start object the vector
			//Debug.Log (mcol.gameObject.name +" c_Pc_distance XY" + c_Pc_distance);
			float xOverlaping = mcol.myBox_extend.x + playerBox_extend.x - Mathf.Abs(c_Pc_distance.x) ; //if positiv value then overlaping, if negative ther is no overlaping
			float yOverlaping = mcol.myBox_extend.y + playerBox_extend.y - Mathf.Abs(c_Pc_distance.y) ; //if positiv value then overlaping, if negative ther is no overlaping
			/*
			if ((playerBox_min.y < mcol.myBox_max.y) && (playerBox_min.x < mcol.myBox_max.x) && (playerBox_max.x > mcol.myBox_min.x) && (playerBox_max.y > mcol.myBox_min.y) ) 
			{
				Debug.Log ("COLLISION");
				transform.position = new Vector3 (transform.position.x, (mcol.myBox_center.y + mcol.myBox_extend.y + playerBox_extend.y) / (playerSprite.pixelsPerUnit), transform.position.z);
				velocityVector.y = 0f;
				groundCollision = true;
			}
			*/
			if (xOverlaping >= 0.0f && yOverlaping >= 0.0f) {
				//Debug.Log (mcol.gameObject.name + "COLISSION" + c_Pc_distance + " xOverlaping:" + xOverlaping + " yOverlaping:" + yOverlaping);
				//wich overlap is biger
				if (xOverlaping > yOverlaping) {
					if (c_Pc_distance.y >= 0f) {
						//Debug.Log ("COLLISION ground");
						transform.position = new Vector3 (transform.position.x, (mcol.myBox_center.y + mcol.myBox_extend.y + playerBox_extend.y + 1.0f) / (toPixels), transform.position.z);
						velocityVector.y = 0.0f;
						groundCollision = true;
					} else if (c_Pc_distance.y < 0.0f) {
						
						//Debug.Log ("COLLISION siling" + "c_Pc_distance.y:" + c_Pc_distance.y);
						transform.position = new Vector3 (transform.position.x, (mcol.myBox_center.y - mcol.myBox_extend.y - playerBox_extend.y - 1.0f) / (toPixels), transform.position.z);
						velocityVector.y = 0.0f;
						topCol = true;
					}
				} else if (yOverlaping > xOverlaping) {
					
					if (c_Pc_distance.x < 0.0f) {
						//Debug.Log ("COLLISION wall - from left side of the wall");
						transform.position = new Vector3 ((mcol.myBox_center.x - mcol.myBox_extend.x - playerBox_extend.x - 1.0f) / (toPixels), transform.position.y, transform.position.z);
						velocityVector.x = 0.0f;
						righthandCol = true;


					} else if (c_Pc_distance.x > 0f) {
						//Debug.Log ("COLLISION wall - from right  side of the wall");
						transform.position = new Vector3 ((mcol.myBox_center.x + mcol.myBox_extend.x + playerBox_extend.x + 1.0f) / (toPixels), transform.position.y, transform.position.z);
						velocityVector.x = 0.0f;

						lefthandCol = true;

					}
					//transform.position = new Vector3 (transform.position.x, (mcol.myBox_center.y + mcol.myBox_extend.y + playerBox_extend.y) / (playerSprite.pixelsPerUnit), transform.position.z);
					//velocityVector.y = 0f;
					//groundCollision = true;

				} 

			} else {

				//Debug.Log (mcol.gameObject.name+ "NO collision");
				//groundCollision = false;
				lefthandCol = false;
				righthandCol = false;
				topCol = false;
				if (velocityVector.y < -0.5f) {
					groundCollision = false;
				}
			}



		}

	}
	void PlayerBoxInit()
	{	
		playerboxScale = 0.5f;
		playerBox_center = new Vector2 (gameObject.transform.position.x * playerSprite.pixelsPerUnit , gameObject.transform.position.y * playerSprite.pixelsPerUnit);
		playerBox_width = playerSprite.rect.width * playerboxScale;
		playerBox_height = playerSprite.rect.height ;
		playerBox_extend = new Vector2 (playerBox_width / 2 * gameObject.transform.lossyScale.x, playerBox_height / 2 * gameObject.transform.lossyScale.y);
		playerBox_max = playerBox_center + playerBox_extend;
		playerBox_min = playerBox_center - playerBox_extend;
		//Debug.Log (gameObject.name + " playerBox parameters(PIXELS X Y): " + " center: " + playerBox_center + " max: " + playerBox_max + " min: " + playerBox_min);
	}
	void PlayerBoxUpdate()
	{
		playerBox_center = new Vector2 (gameObject.transform.position.x * playerSprite.pixelsPerUnit , gameObject.transform.position.y * playerSprite.pixelsPerUnit);
		playerBox_max = playerBox_center + playerBox_extend;
		playerBox_min = playerBox_center - playerBox_extend;
	}
}
