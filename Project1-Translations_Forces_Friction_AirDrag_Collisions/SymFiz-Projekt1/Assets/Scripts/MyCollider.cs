﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(SpriteRenderer))]
public class MyCollider : MonoBehaviour {


	// In rectangle x axis is to right, y axis is to buttom   / oposit to the word coordinates (word coordinates y is up)



	public Sprite mySprite;



	public Vector2 myBox_min;
	public Vector2 myBox_max;
	public Vector2 myBox_center;
	public float myBox_width;
	public float myBox_height;
	public Vector2 myBox_extend;


	// Use this for initialization
	void Start () {
		mySprite = gameObject.GetComponent<SpriteRenderer> ().sprite;


		myBox_center = new Vector2 (gameObject.transform.position.x * mySprite.pixelsPerUnit , gameObject.transform.position.y * mySprite.pixelsPerUnit);
		myBox_width = mySprite.rect.width;
		myBox_height = mySprite.rect.height;
		myBox_extend = new Vector2 (myBox_width / 2 * gameObject.transform.localScale.x, myBox_height / 2 * gameObject.transform.localScale.y);
		myBox_max = myBox_center + myBox_extend;
		myBox_min = myBox_center - myBox_extend;
		Debug.Log (gameObject.name + " myBox parameters(PIXELS X Y): " + " center: " + myBox_center + " max: " + myBox_max + " min: " + myBox_min);






	



		/*
		Debug.Log ( gameObject.name + " center of the rect " + gameObject.GetComponent<SpriteRenderer> ().sprite.rect.center  );
		Debug.Log ( gameObject.name + "bounds.extends" + gameObject.GetComponent<SpriteRenderer> ().sprite.bounds.extents  );
		Debug.Log ( gameObject.name + "bounds.max" + gameObject.GetComponent<SpriteRenderer> ().sprite.bounds.max  );
		Debug.Log ( gameObject.name + "bounds.min" + gameObject.GetComponent<SpriteRenderer> ().sprite.bounds.min  );
		Debug.Log ( gameObject.name + " border size X Y Z W " + gameObject.GetComponent<SpriteRenderer> ().sprite.border  );
		Debug.Log ( gameObject.name + " pixelPerUnit * x  + pixel per Unit * y " + gameObject.GetComponent<SpriteRenderer> ().sprite.pixelsPerUnit * gameObject.transform.position.x + " y= " +
			gameObject.GetComponent<SpriteRenderer> ().sprite.pixelsPerUnit * gameObject.transform.position.y
		);
		*/
		//Debug.Log ( gameObject.name + " UV " + gameObject.GetComponent<SpriteRenderer> ().sprite.uv );
		//Debug.Log ( gameObject.name + " vertices position  " + gameObject.GetComponent<SpriteRenderer> ().sprite.vertices );
		//Debug.Log ( gameObject.name + " vertices position  " + gameObject.GetComponent<SpriteRenderer> ().sprite.vertices );

		Debug.Log (gameObject.name + " POZYCJA OBIEKTU W SWIECIE " + gameObject.transform.position);

	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ( gameObject.name + gameObject.GetComponent<SpriteRenderer> ().sprite.rect.center  );
	}
}
