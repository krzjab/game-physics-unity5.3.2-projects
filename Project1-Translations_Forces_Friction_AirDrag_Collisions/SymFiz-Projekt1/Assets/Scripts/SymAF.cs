﻿
using UnityEngine;
using System.Collections;

[RequireComponent (typeof(SymPE))]
public class SymAF : MonoBehaviour {

	public Vector3 forceVector;
	private SymPE SymPhysicsEngine;

	// Use this for initialization
	void Start () {
		forceVector.x = 0;
		forceVector.y = -10;
		forceVector.z = 0;

		SymPhysicsEngine = GetComponent<SymPE> ();

	}

	void FixedUpdate(){
		if (gameObject.transform.position.y < GetComponent<SymPE>().FLOOR_const) {

		} else {


			SymPhysicsEngine.AddForce (forceVector);
		}
	}
}
