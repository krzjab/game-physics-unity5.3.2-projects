﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinZone : MonoBehaviour {

	public GameObject winZoneObj;
	public GameObject targetToWin;
	public Text textBox;
	public Vector2 distanceToWinVector;
	public float distanceToWin;

	public float helpXdis;
	public float helpYdis;

	public Vector2 heroStartPos;

	// Use this for initialization
	void Start () {
		winZoneObj = gameObject;
		targetToWin = GameObject.FindGameObjectWithTag ("Player");
		textBox = GameObject.FindGameObjectWithTag ("TextBox").GetComponent<Text>();

		heroStartPos = new Vector2 (targetToWin.transform.position.x, targetToWin.transform.position.y);

		distanceToWin = 2.0f;
		distanceToWinVector = new Vector2 (distanceToWin, distanceToWin);
	}
	
	// Update is called once per frame
	void Update () {
		helpXdis = Mathf.Abs (targetToWin.transform.position.x - winZoneObj.transform.position.x);
		helpYdis = Mathf.Abs( targetToWin.transform.position.y - winZoneObj.transform.position.y);

		//Debug.Log ("Distance from winZone to the Player: XY" + helpXdis + " " + helpYdis);

		if ( (helpXdis < distanceToWinVector.x ) &&  (helpYdis < distanceToWinVector.y)) {

				textBox.text = "WINNER!";

		}
	}

	void RestartGame()
	{
		targetToWin.transform.position = new Vector3 (heroStartPos.x, heroStartPos.y, 0.0f);
		textBox.text = "Follow ->";
		targetToWin.GetComponent<SymPE> ().velocityVector = new Vector3 (0.0f, 0.0f, 0.0f);
	}

	void QuitGame()
	{
		Application.Quit ();
	}

}
