﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeadZone : MonoBehaviour {

	public GameObject deadZoneObj;
	public GameObject targetToKill;
	public Text textBox;


	// Use this for initialization
	void Start () {
		deadZoneObj = gameObject;
		targetToKill = GameObject.FindGameObjectWithTag ("Player");
		textBox = GameObject.FindGameObjectWithTag ("TextBox").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if (targetToKill.transform.position.y < deadZoneObj.transform.position.y) {

			textBox.text = "GAME OVER";

		}
	
	}
}
