﻿using UnityEngine;
using System.Collections;

public class zad5 : MonoBehaviour {

	public GameObject sphere1;
	public GameObject sphere2;
	public GameObject sphere3;

	public Vector3 position_point1;
	public Vector3 position_point2;
	public Vector3 position_point3;

	public Vector3 velocity_point1;
	public Vector3 velocity_point2;
	public Vector3 velocity_point3;

	public Vector3 angularVelocity_w;

	public Vector3 distanceFromRotAxis_R_point1;
	public Vector3 distanceFromRotAxis_R_point2;
	public Vector3 distanceFromRotAxis_R_point3;

	public Vector3 centerOfRotationTransform;


	public float[,] angularVelocity_wStar_matrix;

	public  Vector3 L_moment_pedu;
	public float[,] R_rotation_Matrix;

	public Vector3 F_force_vector;

	public float[,] I_matrix_inversed; // change in the update function
	public float[,] I0_matrix_inversed; //input it manualy in the code



	private Mesh triangleMesh;


	private static float[,] Matrix111 = {
		{ 1f, 0f, 0f },
		{ 0f, 1f, 0f }, 
		{ 0f, 0f, 1f }

	};

	private static float[,] Matrix000 = {
		{ 0f, 0f, 0f },
		{ 0f, 0f, 0f }, 
		{ 0f, 0f, 0f }

	};

	// Use this for initialization
	void Start () {


		centerOfRotationTransform = new Vector3 (0f,-2f,-1f);


		sphere1 = GameObject.Find("Sphere1");
		sphere2 = GameObject.Find ("Sphere2");
		sphere3 = GameObject.Find ("Sphere3");

		sphere1.transform.Translate (centerOfRotationTransform);
		sphere2.transform.Translate (centerOfRotationTransform);
		sphere3.transform.Translate (centerOfRotationTransform);

		position_point1 = sphere1.transform.position;
		position_point2 = sphere2.transform.position;
		position_point3 = sphere3.transform.position;

		distanceFromRotAxis_R_point3 = position_point3;
		distanceFromRotAxis_R_point2 = position_point2;
		distanceFromRotAxis_R_point1 = position_point1;

		F_force_vector = new Vector3 (0f,1f,0f);

		L_moment_pedu = new Vector3 (0f, 0f, 0f);

		R_rotation_Matrix = Matrix111;

		I0_matrix_inversed = new float[3, 3] { { 0.03125f, 0f, 0f },
			{ 0f, 1.148148f, -0.259259f }, 
			{ 0f, -0.259259f, 0.037037f }
		};

		angularVelocity_wStar_matrix = new float[3, 3] { { 0f, -angularVelocity_w.z, angularVelocity_w.y },
			{ angularVelocity_w.z, 0f, -angularVelocity_w.x }, 
			{ -angularVelocity_w.y, angularVelocity_w.x, 0f }
		};


		//wpisz wartości odpowiednie do macierzy z obliczen manualnych
		I_matrix_inversed = I0_matrix_inversed;

		MakeCube();

		angularVelocity_w = new Vector3 (0f, 0f, 0f) ;


	}

	// Update is called once per frame
	void Update () {
		UpdateTriangleMesh ();
	}
	void FixedUpdate()
	{
		//calculations for force on point1
		//TriangleRotationEulerFunction_withForces (distanceFromRotAxis_R_point1);
		//Charakterystyka ruchu poprawiona przez zmiane argumenty na taki ktory zmienia sie w czasie (Aby prawidolowo wyliczyć moment sił w momencie pędu)
		TriangleRotationEulerFunction_withForces (position_point1);

	}

	//get vertices to the triangle
	private Vector3[] GetVertices()
	{

		Vector3[] vertices = new Vector3[]
		{
			sphere1.transform.position, sphere2.transform.position, sphere3.transform.position
		};

		return vertices;
	}

	private Vector3[] GetNormals()
	{
		Vector3 left = Vector3.left;

		Vector3[] normales = new Vector3[] {
			left, left, left
		};

		return normales;
	}

	private Vector2[] GetUVsMap()
	{
		Vector2 _00_CORDINATES = new Vector2 (0f, 0f);
		Vector2 _10_CORDINATES = new Vector2 (1f, 0f);
		Vector2 _01_CORDINATES = new Vector2 (0f, 1f);

		Vector2[] uvs = new Vector2[] {
			_00_CORDINATES, _01_CORDINATES, _10_CORDINATES,

		};

		return uvs;
	}

	private int[] GetTriangles()
	{
		//one triangle?
		int[] triangles = new int[] {
			0,1,2,
			2,1,0
		};
		return triangles;
	}

	private Mesh GetCubeMesh()
	{

		if (GetComponent<MeshFilter>() == null)
		{
			Mesh mesh;
			MeshFilter filter = gameObject.AddComponent<MeshFilter>();
			mesh = filter.mesh;
			mesh.Clear();
			return mesh;
		}
		else
		{
			return gameObject.AddComponent<MeshFilter>().mesh;
		}
	}

	/// <summary>
	/// Makes the Cube
	/// </summary>
	void MakeCube()
	{
		triangleMesh = GetCubeMesh();
		triangleMesh.vertices = GetVertices();
		triangleMesh.normals = GetNormals();
		triangleMesh.uv = GetUVsMap();
		triangleMesh.triangles = GetTriangles();
		triangleMesh.RecalculateBounds();
		triangleMesh.RecalculateNormals();
		triangleMesh.Optimize();
	}


	public void Update_wStar_matrix()
	{
		angularVelocity_wStar_matrix = new float[3,3] { { 0f, -angularVelocity_w.z, angularVelocity_w.y },
			{ angularVelocity_w.z, 0f, -angularVelocity_w.x }, 
			{ -angularVelocity_w.y, angularVelocity_w.x, 0f }
		};
	}

	public void UpdateTriangleMesh()
	{
		Mesh mesh = GetComponent<MeshFilter>().mesh;
		mesh.Clear();
		mesh.vertices = GetVertices();
		mesh.uv = GetUVsMap();
		mesh.triangles = GetTriangles();
	}
		

	public void TriangleRotationEulerFunction_withForces( Vector3 R0_vector)
	{
		//main equasions
		L_moment_pedu = L_moment_pedu + Vector3.Cross (MultiplyMatrixVector3 (R_rotation_Matrix, R0_vector), F_force_vector) * Time.deltaTime;
		R_rotation_Matrix = AddMatrixToMatrix (R_rotation_Matrix , MultiplyMatrixScalar(  MultiplyMatrix (angularVelocity_wStar_matrix, R_rotation_Matrix) , Time.deltaTime));
		I_matrix_inversed = MultiplyMatrix (MultiplyMatrix (TransposeMatrix (R_rotation_Matrix), I0_matrix_inversed), R_rotation_Matrix);
		angularVelocity_w = MultiplyMatrixVector3 (I_matrix_inversed, L_moment_pedu);



		//transforms
		Update_wStar_matrix ();
		velocity_point1 = MultiplyMatrixVector3 (angularVelocity_wStar_matrix, position_point1);
		velocity_point2 = MultiplyMatrixVector3 (angularVelocity_wStar_matrix, position_point2);
		velocity_point3 = MultiplyMatrixVector3 (angularVelocity_wStar_matrix, position_point3);

		position_point1 = position_point1  + velocity_point1 * Time.deltaTime;
		sphere1.transform.position = position_point1 ;

		position_point2 = position_point2  + velocity_point2 * Time.deltaTime;
		sphere2.transform.position = position_point2 ;

		position_point3 = position_point3  + velocity_point3 * Time.deltaTime;
		sphere3.transform.position = position_point3 ;



		//	Debug.Log ("punkt3 pozycja" + position_point3 + "sfera 3 pozycja" + sphere3.transform.position);
		//	Debug.Log ("predkosc kątowa: " + angularVelocity_w  +" w czasie " + Time.time);


		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				//Debug.Log ("Rotation Matrix" + "[" + i + "," + j +"] = " + R_rotation_Matrix [i, j]);
			}
		}
	}

	public float[,] TransposeMatrix(float[,] matrix) {
		float[,] ReturnMatrix, HelpMatrix = Matrix000;


		ReturnMatrix = matrix;

		HelpMatrix[0,1] = ReturnMatrix[ 0,1];  ReturnMatrix[ 0,1] = ReturnMatrix[ 1,0];  ReturnMatrix[ 1,0] = HelpMatrix[0,1];
		HelpMatrix[0,2] = ReturnMatrix[ 0,2];  ReturnMatrix[ 0,2] = ReturnMatrix[ 2,0];  ReturnMatrix[ 2,0] = HelpMatrix[0,2];
		HelpMatrix[1,2] = ReturnMatrix[ 1,2];  ReturnMatrix[ 1,2] = ReturnMatrix[2,1];  ReturnMatrix[2,1] = HelpMatrix[1,2];

		return ReturnMatrix;
	}

	public void TestAddMatrix()
	{
		float[,] sumMatrix = new float[3,3];
		sumMatrix = AddMatrixToMatrix (R_rotation_Matrix, I0_matrix_inversed);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				//Debug.Log ("Rotation Matrix" + "[" + i + "," + j +"] = " + sumMatrix [i, j]);
			}
		}
	}

	public Vector3 MultiplyMatrixVector3(float[,] matrix3x3 , Vector3 vector3 ){

		Vector3 returnVector3;

		returnVector3.x = matrix3x3 [0, 0] * vector3.x + matrix3x3 [0, 1] * vector3.y + matrix3x3 [0, 2] * vector3.z;
		returnVector3.y = matrix3x3 [1, 0] * vector3.x + matrix3x3 [1, 1] * vector3.y + matrix3x3 [1, 2] * vector3.z;
		returnVector3.z = matrix3x3 [2, 0] * vector3.x + matrix3x3 [2, 1] * vector3.y + matrix3x3 [2, 2] * vector3.z;

		return returnVector3;
	}

	public float[,] MultiplyMatrixScalar(float[,] matrix, float scalar){

		//int rA = matrix.GetLength(0);
		//int cA = matrix.GetLength(1);


		float[,] returnMatrix = Matrix000;

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{

				returnMatrix [i, j] = matrix [i, j] * scalar;
			}
		}

		return returnMatrix;

	}

	public float[,] AddMatrixToMatrix(float[,] matrixA , float[,] matrixB)
	{
		//int rA = matrixA.GetLength(0);
		//int cA = matrixA.GetLength(1);


		float[,] returnMatrix = new float[3,3];
		//	float[,] kHasil = new float[rA, cB];
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{

				returnMatrix [i, j] = matrixA [i, j] + matrixB [i,j] ;
			}
		}

		return returnMatrix;
	}

	public float[,] MultiplyMatrix(float[,] A, float[,] B)
	{
		int rA = A.GetLength(0);
		int cA = A.GetLength(1);
		int rB = B.GetLength(0);
		int cB = B.GetLength(1);
		float temp = 0;
		//float[,] kHasil = new float[,][rA, cB];
		float[,] kHasil = new float[rA, cB];
		if (cA != rB)
		{
			Debug.Log("matrik can't be multiplied !!");
			return A; // just for debuging , no use
		}
		else
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					temp = 0;
					for (int k = 0; k < 3; k++)
					{
						temp += A[i, k] * B[k, j];
					}
					kHasil[i, j] = temp;
				}
			}
			return kHasil;
		}
	}

}



