﻿using UnityEngine;
using System.Collections;

public class TriangleMeshGenerator : MonoBehaviour {

	public GameObject sphere1;
	public GameObject sphere2;
	public GameObject sphere3;

	public Vector3 position_point1;
	public Vector3 position_point2;
	public Vector3 position_point3;

	public Vector3 velocity_point1;
	public Vector3 velocity_point2;
	public Vector3 velocity_point3;

	public Vector3 angularVelocity_w;

	public Vector3 distanceFromRotAxis_R_point1;
	public Vector3 distanceFromRotAxis_R_point2;
	public Vector3 distanceFromRotAxis_R_point3;

	//public float[,] angulatVelocity_wStar_matrix;

	public float[,] angularVelocity_wStar_matrix;




	private Mesh triangleMesh;

	//bryla sztywna
	public Vector3 x_com_t;
	public Vector3 v_com_t;


	public Vector3 MultiplyMatrixVector3(float[,] matrix3x3 , Vector3 vector3 ){

		Vector3 returnVector3;

		returnVector3.x = matrix3x3 [0, 0] * vector3.x + matrix3x3 [0, 1] * vector3.y + matrix3x3 [0, 2] * vector3.z;
		returnVector3.y = matrix3x3 [1, 0] * vector3.x + matrix3x3 [1, 1] * vector3.y + matrix3x3 [1, 2] * vector3.z;
		returnVector3.z = matrix3x3 [2, 0] * vector3.x + matrix3x3 [2, 1] * vector3.y + matrix3x3 [2, 2] * vector3.z;

		return returnVector3;
	}


	// wiersze, kolumny

	public float[,] Matrix3x3;
	public float[,] Vector3x1;

	public float[,] MultiplyMatrix(float[,] A, float[,] B)
	{
		int rA = A.GetLength(0);
		int cA = A.GetLength(1);
		int rB = B.GetLength(0);
		int cB = B.GetLength(1);
		float temp = 0;
		//float[,] kHasil = new float[,][rA, cB];
		float[,] kHasil = new float[rA, cB];
		if (cA != rB)
		{
			Debug.Log("matrik can't be multiplied !!");
			return A; // just for debuging , no use
		}
		else
		{
			for (int i = 0; i < rA; i++)
			{
				for (int j = 0; j < cB; j++)
				{
					temp = 0;
					for (int k = 0; k < cA; k++)
					{
						temp += A[i, k] * B[k, j];
					}
					kHasil[i, j] = temp;
				}
			}
			return kHasil;
		}
	}

	/*
	private static float[][] identityMatrix =
	{
		new [] {1.0f, 0.0f, 0.0f},
		new []{0.0f, 1.0f, 0.0f},
		new []{0.0f, 0.0f, 1.0f}
	};
	*/
	// Use this for initialization
	void Start () {
		sphere1 = GameObject.Find("Sphere1");
		sphere2 = GameObject.Find ("Sphere2");
		sphere3 = GameObject.Find ("Sphere3");

		position_point1 = sphere1.transform.position;
		position_point2 = sphere2.transform.position;
		position_point3 = sphere3.transform.position;
	
		angularVelocity_wStar_matrix = new float[3, 3] { { 0f, -angularVelocity_w.z, angularVelocity_w.y },
			{ angularVelocity_w.z, 0f, -angularVelocity_w.x }, 
			{ -angularVelocity_w.y, angularVelocity_w.x, 0f }
		};



		//angular welocity gwiazdka matrix
		Matrix3x3 = new float[3, 3] { { 0f, -angularVelocity_w.z, angularVelocity_w.y },
			{ angularVelocity_w.z, 0f, -angularVelocity_w.x }, 
			{ -angularVelocity_w.y, angularVelocity_w.x, 0f }
		};
		// int[,] array2Da = new int[4, 2] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } };
		MakeCube();
	}
	
	// Update is called once per frame
	void Update () {
		UpdateTriangleMesh ();
	}
	void FixedUpdate()
	{
		TriangleRotationEulerFunction ();
	}

	//get vertices to the triangle
	private Vector3[] GetVertices()
	{

		Vector3[] vertices = new Vector3[]
		{
			//position_point1, position_point2, position_point3
			sphere1.transform.position, sphere2.transform.position, sphere3.transform.position
		};

		return vertices;
	}

	private Vector3[] GetNormals()
	{
		Vector3 left = Vector3.left;

		Vector3[] normales = new Vector3[] {
			left, left, left
		};

		return normales;
	}

	private Vector2[] GetUVsMap()
	{
		Vector2 _00_CORDINATES = new Vector2 (0f, 0f);
		Vector2 _10_CORDINATES = new Vector2 (1f, 0f);
		Vector2 _01_CORDINATES = new Vector2 (0f, 1f);
//		Vector2 _11_CORDINATES = new Vector2 (1f, 1f);

		Vector2[] uvs = new Vector2[] {
			_00_CORDINATES, _01_CORDINATES, _10_CORDINATES,

		};

		return uvs;
	}

	private int[] GetTriangles()
	{
		//one triangle?
		int[] triangles = new int[] {
			0,1,2,
			2,1,0
		};
		return triangles;
	}

	private Mesh GetCubeMesh()
	{

		if (GetComponent<MeshFilter>() == null)
		{
			Mesh mesh;
			MeshFilter filter = gameObject.AddComponent<MeshFilter>();
			mesh = filter.mesh;
			mesh.Clear();
			return mesh;
		}
		else
		{
			return gameObject.AddComponent<MeshFilter>().mesh;
		}
	}

	/// <summary>
	/// Makes the Cube
	/// </summary>
	void MakeCube()
	{
		triangleMesh = GetCubeMesh();
		triangleMesh.vertices = GetVertices();
		triangleMesh.normals = GetNormals();
		triangleMesh.uv = GetUVsMap();
		triangleMesh.triangles = GetTriangles();
		triangleMesh.RecalculateBounds();
		triangleMesh.RecalculateNormals();
		triangleMesh.Optimize();
	}


	public void Update_wStar_matrix_zad1()
	{
		angularVelocity_wStar_matrix = new float[3,3] { { 0f, -angularVelocity_w.z, angularVelocity_w.y },
			{ angularVelocity_w.z, 0f, -angularVelocity_w.x }, 
			{ -angularVelocity_w.y, angularVelocity_w.x, 0f }
		};
	}
	public void Update_wStar_matrix_zad2()
	{

	}

	public void UpdateTriangleMesh()
	{
		Mesh mesh = GetComponent<MeshFilter>().mesh;
		mesh.Clear();
		mesh.vertices = GetVertices();
		mesh.uv = GetUVsMap();
		mesh.triangles = GetTriangles();
	}

	public void TriangleRotationEulerFunction()
	{
		//zad 2 angularVelocity
		//angularVelocity_w = new Vector3 (1f, 1f, 0f) * Time.time;

		//zad 1 angularVelocity
		angularVelocity_w = new Vector3 (1f, 1f, 0f) ;

		Update_wStar_matrix_zad1 ();

		velocity_point1 = new Vector3 (0f,0f,0f);
		velocity_point2 = MultiplyMatrixVector3 (angularVelocity_wStar_matrix, position_point2);
		velocity_point3 = MultiplyMatrixVector3 (angularVelocity_wStar_matrix, position_point3);

		position_point1 = position_point1 + velocity_point1 * Time.deltaTime;
		sphere1.transform.position = position_point1;

		position_point2 = position_point2 + velocity_point2 * Time.deltaTime;
		sphere2.transform.position = position_point2;

		position_point3 = position_point3 + velocity_point3 * Time.deltaTime;
		sphere3.transform.position = position_point3;

		Debug.Log ("punkt3 pozycja" + position_point3 + "sfera 3 pozycja" + sphere3.transform.position);
		Debug.Log ("predkosc kątowa: " + angularVelocity_w  +" w czasie " + Time.time);
	}
}
