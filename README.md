# Game Physics Unity5.3.2 Projects

(Projects from 2016)

Repository contains Unity5.3.2 projects with game physics exercises.

**Project 1: About**

Project 1 contains scripts with: Forces, Translations, Friction, AirDrag, Collisions

**Project 1: Scripts Locations**

Project1-Translations_Forces_Friction_AirDrag_Collisions/SymFiz-Projekt1/Assets/Scripts...

**Project 2: About**

Project 2 contains scripts which implement concepts about rotating discrete rigid bodies.

**Project 2: Scripts Locations**

Project2-rotation_discrete_rigid_body/Assets/Scripts...